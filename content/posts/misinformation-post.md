---
title: Misinformation
date: 2021-08-19
published: true
tags: ["Markdown", "Post"]
canonical_url: false
description: "How the really bad and wrong information spreads the fastest and gets the most recognition"
---

<b>Information</b>.

We all need it. It is easily available in these <i>digital</i> times thanks to the easy access to the internet.
But that doesn't mean all the information out there on the internet is <b>Correct</b> (contrary to what alot of people may think). Google and other search engines do not necessarily filter the data available to only display correct information and that is due to a larger number of factors(topic for another day or person entirely). Hence, not all info on the internet is to be trusted as accurate.

Another common source of misinformation is the so-called <i>expert</i>. Now to clarify, am not talking about actual experts like people who have actually been trained in a particular field like an actual Doctor or Physicist. Am talking about people who think they know a whole lot about stuff they actually know nothing (true) about. This also tends to be a person of status or authority in a big office who people may be lloking to have knowledge in a certain area but as we know commonly today, now everyone know what they are doing just because thry have a nice title or office.
A bunch of people fall in this category. I am constantly amazed by the information I hear people give out so freely. So many want to sound so smart but end up sounding so (for lack of a less insulting word), <b>really stupid</b>.
I am a nutritionst by training, but you wouldnt't believe the "remedies" or "advice" people have tried to give, and not just to me but to others in the many places I have been.

    Ignorance is bliss

But to whom?

<b>Is it worth it to let the person go on and on propagating this less than accurate information to so many minds that take it as truth and further propel it into other minds</b>
